# sar-core

## Development

To download this repository, if you have an RSA key, in a terminal and a convenient location, type:
```
mkdir -p sar/common-tools/python-modules
cd sar/common-tools/python-modules
git clone git@gitlab.com:search-and-rescue/common-tools/modules/sar-core.git
cd sar-core
```
if you dont have an RSA key change git clone line for
```
git clone https://gitlab.com/search-and-rescue/common-tools/modules/sar-core.git
```

Install SAR modules with
```
sudo pip3 install -U . --break-system-packages
```

You can test modules using test app in `python/test` directory
