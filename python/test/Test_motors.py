#! /usr/bin/env python3

"""device_pca9685

This module is used as abstraction layer of PCA9685 pwm driver to send.
This values control iHead's servo orientation and light intensity.
"""

import time
import math
import logging

from sarcore.modules.interface_bus_i2c import BusI2C
from sarcore.modules.interface_gpio import GPIO


class Pca9685:
    """Class to use PCA9685 pwm driver

    This module can control up to 16 pwm channels

    Note:
        Details on each registers can be found at
        corresponding sensor datasheet and
        `Register Map <https://www.nxp.com/docs/en/data-sheet/PCA9685.pdf>`_

    Args:
        i2c_port: i2c port where sensor is available

    Attributes:

    """

    address = 0x40
    """i2c address of PCA9685

    All i2c bus operations involving PCA9685 should be done pointing to this
    address.
    """

    osc_clk = 25000000.0 # 25MHz
    """oscillator frequency

    internal 25 MHz oscillator.
    """

    mode1_address = 0x00
    """Mode register 1

    8 bit config register
    """

    prescale_address = 0xFE
    """Prescaler address for PWM output frequency

    programmable prescaler to adjust the PWM pulse widths of multiple devices.
    """

    prescale_min = 3
    """Prescaler max value
    """

    prescale_max = 255
    """Prescaler min value
    """

    mode1_sleep_bit = 0x10   # 0b00010000
    """Sleep bit register

    1: Normal mode
    0: Low power mode. Oscillator off
    """

    mode1_restart_bit = 0x80 # 0b10000000
    """Restart bit register

    If the PCA9685 is operating and the user decides to put the chip to sleep without stopping
    any of the PWM channels, the RESTART bit will be set to logic 1 at the end of the PWM
    refresh cycle. The contents of each PWM register are held valid when the clock is off.

    0: Restart disabled
    1: Restart enabled
    """

    led0_on = 0x06
    """LED0_ON count for LED0

    LED control register
    """

    led0_on_low = 0x06
    """LED0_ON_LOW part

    LED control register lower bits
    """

    led0_on_high = 0x07
    """LED0_ON_HIGH part

    LED control register high bits
    """

    led0_off = 0x08
    """LED0_OFF count for LED0

    LED control register
    """

    led0_off_low = 0x08
    """LED0_OFF_LOW part

    LED control register lower bits
    """

    led0_off_high = 0x09
    """LED0_OFF_HIGH part

    LED control register high bits
    """

    whoami = 0xE0
    """i2c address of modules register

    This register is used to verify the identity of the device
    """

    whoami_ans = 0x00
    """default value of the PCA9685 registry
    """


    def __init__(self, i2c_port=1):

        self.i2c = BusI2C(i2c_port)

        self.gpio = GPIO()

        self._output_enable_pin = 7
        self._pwm_frequency = 1000

        self._discovered = False
        self.model = 'PCA9685'
        self.setup()

    def discover(self):
        reg = self.i2c.write_byte(Pca9685.address, Pca9685.mode1_address, 0x80) # Reset

        if reg == True:
            logging.info('PCA9685 module was found.')
            self._discovered = True
        else:
            logging.info('PCA9685 module was not found.')

    @property
    def discovered(self):
        return self._discovered

    def setup(self):
        self.i2c.write_byte(Pca9685.address, Pca9685.mode1_address, 0x80) # Reset
        time.sleep(0.1)

        self.set_frequency(self._pwm_frequency)
        self.set_freq(self._pwm_frequency)

        self.gpio.setup(self._output_enable_pin, 'out')

    def set_frequency(self, frequency):
        """Set PWM frequency

        """
        self._pwm_frequency = frequency

    def get_pwm_frequency(self):
        """Get PWM frequency

        """
        return self._pwm_frequency


    def set_duty(self, channel, duty):
        """Set channel PWM duty

        Args:
            channel: PCA9685 output channel [0-15]
            duty: duty cycle [0-100]
        """

        if isinstance(duty, list):
            duty = duty[0]
        if isinstance(duty, tuple):
            duty = duty[0]

        if duty > 100:
            duty = 100.0
        elif duty < 0:
            duty = 0.0

        time_on = 0
        time_off = 100

    def set_channel_on_off(self, channel, time_on, time_off):
        """Set channel on off time

        Set ON OFF values on the specified channel
        Args:
            channel: PCA9685 output channel [0-15]
            time_on: ON count. Can vary from 0 to 4095
            time_off: ON count. Can vary from 0 to 4095
        """
        self.i2c.write_byte(Pca9685.address, Pca9685.led0_on_low   + 4 * channel, time_on & 0xFF)
        self.i2c.write_byte(Pca9685.address, Pca9685.led0_on_high  + 4 * channel, time_on >> 8)
        self.i2c.write_byte(Pca9685.address, Pca9685.led0_off_low  + 4 * channel, time_off & 0xFF)
        self.i2c.write_byte(Pca9685.address, Pca9685.led0_off_high + 4 * channel, time_off >> 8)

    def set_freq(self, freq):
        """Set pwm frequency

        Args:
            freq: PWM frequency output
        """
        if freq < 1:
            freq = 1
        elif freq > 3500:
            freq = 3500 # Datasheet limit is 3052=50MHz/(4*4096)

        prescale_value = Pca9685.osc_clk
        prescale_value /= 4096.0       # 12-bit
        prescale_value /= freq
        prescale_value -= 1.0
        prescale = math.floor(prescale_value + 0.5)

        if prescale < Pca9685.prescale_min:
            prescale = Pca9685.prescale_min
        if prescale > Pca9685.prescale_max:
            prescale = Pca9685.prescale_max

        oldmode = self.i2c.read_byte(Pca9685.address, Pca9685.mode1_address)
        if oldmode is None:
            return
        newmode = (oldmode & ~Pca9685.mode1_restart_bit) | Pca9685.mode1_sleep_bit
        self.i2c.write_byte(Pca9685.address, Pca9685.mode1_address, newmode)
        self.i2c.write_byte(Pca9685.address, Pca9685.prescale_address, prescale)
        self.i2c.write_byte(Pca9685.address, Pca9685.mode1_address, oldmode)
        time.sleep(0.01)
        self.i2c.write_byte(Pca9685.address, Pca9685.mode1_address, oldmode | Pca9685.mode1_restart_bit | 0x20)

        currentmode = self.i2c.read_byte(Pca9685.address, Pca9685.mode1_address)

    def disable_output(self):
        """Disable output PCA9685 gpio pins

        """
        self.gpio.pin_on(self._output_enable_pin)

    def enable_output(self):
        """Enable output PCA9685 gpio pins

        """
        self.gpio.pin_off(self._output_enable_pin)

if __name__ == '__main__':
    pca=Pca9685()
    pca.discover()
    print("status: ", pca._discovered)
    CHANNEL_X=1
    CHANNEL_Y=0
    while 1:
        for off_time in range(500, 3500, 200):
            pca.set_channel_on_off(0, 0, off_time)
            #  pca.set_channel_on_off(1, 0, off_time)
            print(off_time)
            time.sleep(0.5)
