
import os
import shutil

from borescope import bs

def config_generator():
    dst_path = os.path.join(os.environ['HOME'],
            		    '.config',
            		    'borescope')

    src_file = os.path.join(os.path.dirname(os.path.dirname(bs.__file__)),
            		    'borescope',
            		    'borescope.conf')

    os.makedirs(dst_path, exist_ok=True)

    dst_file = os.path.join(dst_path, 'borescope.conf')

    print(f'Trying to generate config file: {dst_file}')
    if os.path.exists(dst_file):
        print('File exists. Leaving it unchanged.')
    else:
        try:
            shutil.copyfile(src_file, dst_file)
            print("File generated successfully.")
        except PermissionError:
            print("Permission denied.")
        except:
            print("Error occurred while generating file.")
            raise
