#! /usr/bin/env python3

import platform

#from sarcore.modules.service_sensors import Sensors
from sarcore.modules.service_actuators import Actuators
from sarcore.modules.config_file_parser import ConfigFileParser

class SARCore:
    def __init__(self):
        config_file = ConfigFileParser(None)

        self.options = config_file.get_options()

        self.running_on_arm = None

        self.scan_arch()

        #self.sensors = Sensors(self.options)
        self.actuators = Actuators(self.options)

    def scan_arch(self):
        if platform.machine() in ['armv7l', 'aarch64']:
            self.options.running_on_arm = True
            self.running_on_arm = True
        else:
            self.options.running_on_arm = False
            self.running_on_arm = False
