#! /usr/bin/env python3

"""driver_servo

This module is used as controller to configure the use of a servo motor. The
parameters that configure are duty cicle and started angle.
"""

from sarcore.modules.device_pca9685 import Pca9685


class Servo:
    """Class to use MG996

    Note:
        Details and main characteristics can be found in
        `datasheet <https://datasheetspdf.com/pdf/942981/ETC/MG996R/1>`__

    Attributes:
        pwm: device class instance
        available (bool): configuration state variable
    """

    # duty cycle percentage for full range


    def __init__(self, options):
        self.options = options

        self.device = None
        self.available = False

        self.device_list = [
                Pca9685,
                ]

        self._channel = None

        self.min_angle = -90
        """Servo phisical min angle
        """

        self.max_angle = 90
        """Servo phisical max angle
        """

        self._angle = None

        self._intensity = 0
        self.disable()

    def discover(self, address=None):
        self.device = None
        if self.options.pwm is True and self.options.running_on_arm:
            for device in self.device_list:
                self.device = device()
                self.device.discover(address)
                if self.device.discovered:
                    self.available = True
                    break
            else:
                self.device = None

            if self.device is not None:
                self.device.setup()

    @property
    def angle(self):
        return self._angle

    @angle.setter
    def angle(self, value):
        self._angle = value
        self.set_angle(value)

    def set_angle(self, angle):
        """Set axis and angle for step of servo

        Args:
            channel (int): X o Y axis value
            angle (int): Step value in degrees
        """
        if self.available:
            if angle > self.max_angle:
                angle = self.max_angle
            elif angle < self.min_angle:
                angle = self.min_angle

            duty = (100/(self.max_angle - self.min_angle))* (angle + self.min_angle) + 100
            self.device.set_duty(self._channel, duty)
            return duty
        return None

    def set_channel(self, channel):
        self._channel = channel

    def set_min_max_angles(self, min_max):
        if type(min_max) is tuple:
            if len(min_max) == 2:
                self.min_angle = min_max[0]
                self.max_angle = min_max[1]

    def disable(self):
        """Disable LEDs

        """

        if self.available:
            self.device.disable_output()

    def enable(self):
        """Enable LEDs

        """

        if self.available:
            self.device.enable_output()

class servo(Servo):
    """Class to use Servo driver

    Note:

    This module is to keep backward compatibility and will be removed on a future refactor
    """
    pass

if __name__ == '__main__':

    from types import SimpleNamespace
    import time
    options = SimpleNamespace()
    options.pwm = True
    servo = Servo(options)
    servo.discover()
    servo.enable()
    print("available:", servo.available)
    demora = 0.005
    servo_limit_min = -70
    servo_limit_max = 70
    while 1:
        for angle in range(-90,90, 1):
            servo.set_channel(servo.channel[0])
            duty = servo.set_angle(angle)
            print(duty, angle)
            time.sleep(demora)
