#! /usr/bin/env python3

from sarcore.modules.driver_servo import Servo


class Actuators:
    def __init__(self, options):
        self.options = options

        self.servo = Servo(self.options)

    def discover(self, device='all', address=None):
        if device == 'servo' or device == 'all':
            self.servo.discover(address)


if __name__ == '__main__':
    from types import SimpleNamespace
    import platform

    options = SimpleNamespace()
    options.pwm = True
    if platform.machine() == 'aarch64':
        options.running_on_arm = True
    else:
        options.running_on_arm = False

    actuators = Actuators(options)
    actuators.discover('servo')

    actuators.servo.enable()
    actuators.servo.set_angle(actuators.servo.channel[1], 90)

