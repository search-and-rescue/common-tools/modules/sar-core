#! /usr/bin/env python3

from pyPS4Controller.controller import Controller


class JoyStick(Controller):
    def __init__(self, parent, **kwargs):
        Controller.__init__(self, **kwargs)

        self.parent = parent

    def on_x_press(self):
        if self.parent is not None:
            self.parent.signal_update('x_press')

    def on_x_release(self):
        pass

    def on_triangle_release(self):
        pass

    def on_triangle_press(self):
        if self.parent is not None:
            self.parent.signal_update('triangle_press')

    def on_triangle_release(self):
        pass

    def on_square_press(self):
        if self.parent is not None:
            self.parent.signal_update('square_press')

    def on_square_release(self):
        pass

    def on_circle_press(self):
        pass

    def on_circle_release(self):
        pass

    def on_L1_press(self):
        pass

    def on_L1_release(self):
        pass

    def on_L2_press(self, value):
        pass

    def on_L2_release(self):
        pass

    def on_R1_press(self):
        pass

    def on_R1_release(self):
        pass

    def on_R2_press(self, value):
        pass

    def on_R2_release(self):
        pass

    def on_up_arrow_press(self):
        pass

    def on_up_down_arrow_release(self):
        pass

    def on_down_arrow_press(self):
        pass

    def on_left_arrow_press(self):
        pass

    def on_left_right_arrow_release(self):
        pass

    def on_right_arrow_press(self):
        pass

    def on_L3_up(self, value):
        pass

    def on_L3_down(self, value):
        pass

    def on_L3_left(self, value):
        pass

    def on_L3_right(self, value):
        pass

    def on_L3_release(self):
        pass

    def on_R3_up(self, value):
        pass

    def on_R3_down(self, value):
        pass

    def on_R3_left(self, value):
        pass

    def on_R3_right(self, value):
        pass

    def on_R3_release(self):
        pass

    def on_options_press(self):
        pass

    def on_options_release(self):
        pass

    def on_R3_y_at_rest(self):
        pass


if __name__ == '__main__':
    controller = JoyStick(parent=None, interface="/dev/input/js0", connecting_using_ds4drv=False)
    controller.listen(timeout=60)
