#! /usr/bin/env python3

from sarcore.modules.driver_camera import Camera


class Sensors:
    def __init__(self, options, device='all'):
        self.options = options

        if device in ['camera', 'all']:
            self.camera = Camera(self.options)

    def discover(self, device='all'):
        if device in ['camera', 'all']:
            self.options.camera = self.camera.set_capturing_device()
