#! /usr/bin/env python3

import cv2
import numpy as np


class Camera:
    def __init__(self, options):
        self.options = options

        self.frame = None
        self.grey_frame = None
        self.cap = None
        self.device = None
        self.fps = 0.0
        self._is_active = False

        self.init_frame()

    def init_frame(self):
        self.grey_frame = np.zeros((
            self.options.screen_height,
            self.options.screen_width,
            self.options.channels), np.uint8) + 100

        #self.grey_frame = cv2.imread('/home/claudiojpaz/.config/borescope/debris.jpg')

        self.frame = np.copy(self.grey_frame)

    def set_capturing_device(self):
        self._is_active = True
        self.device = self.set_cap_video()
        if not self.device:
            self.device = self.set_emu_video()
            return False

        return self.device

    def set_cap_video(self):
        if type(self.options.camerapath) is str:
            print(f'Trying to capture from {self.options.camerapath}')
            attempts = 3
            for attempt in range(attempts):
                self.cap = cv2.VideoCapture(self.options.camerapath)
                if self.cap is not None and self.cap.isOpened():
                    print(f'Device {self.options.camerapath} is seted.')
                    break
            else:
                print(f'After {attempts} attempts device can not be opened.')
                self.options.camerapath = False

        return self.options.camerapath

    def set_emu_video(self):
        return 'emulated'

    def set_frame(self):
        if type(self.options.camerapath) is str:
            if self._is_active and self.cap.isOpened():
                # Capture frame-by-frame
                ret, frame = self.cap.read()
                if ret is False:
                    print("Error getting frame")
                    frame = None
                else:
                    frame = cv2.resize(frame,
                        (self.options.screen_width,
                         self.options.screen_height),
                        fx=0,
                        fy=0,
                        interpolation=cv2.INTER_CUBIC)
            else:
                frame = None

        else:
            frame = None

        if frame is None:
            frame = np.copy(self.grey_frame)

        self.frame = frame

    def get_rgb_frame(self):
        return cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)

    def release_capturing_device(self):
        self._is_active = False
        if self.device != 'emulated':
            self.cap.release()
            print('Capture device was release.')

    @property
    def is_active(self):
        return self._is_active
