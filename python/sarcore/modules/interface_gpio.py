#! /usr/bin/env python3

import logging


class GPIO:
    def __init__(self):
        try:
            from RPi import GPIO as io
            self._io = io

        except RuntimeError:
            logging.info('GPIO is not active. Running on desktop')
            self._io = None

        if self._io is not None:
            # setup
            self._io.setmode(self._io.BOARD)
            self._io.setwarnings(False)
            print('GPIO is active')

    def setup(self, pin, mode):
        if self._io is not None:
            if mode == 'out':
                self._io.setup(pin, self._io.OUT)
            if mode == 'in':
                self._io.setup(pin, self._io.IN)

    def pin_on(self, pin):
        if self._io is not None:
            self._io.output(pin, self._io.HIGH)

    def pin_off(self, pin):
        if self._io is not None:
            self._io.output(pin, self._io.LOW)

