#! /usr/bin/env python3

import logging
import smbus2


class BusI2C:
    def __init__(self, port):
        self.open = False
        self.bus = self.bus_init(port)

    def bus_init(self, port):
        if port is None:
            return None

        try:
            self.open = True
            bus = smbus2.SMBus(port)
            logging.info('I2C port is ready to use.')
            return bus
        except FileNotFoundError:
            self.open = False
            logging.error('Not able to open port.')
            return None

    def write_byte(self, address, offset, data):
        try:
            self.bus.write_byte_data(address, offset, data)
            return True
        except AttributeError:
            logging.error('Port is not open')
            return None
        except OSError as error:
            logging.error('Not able to write.')
            logging.error(f'{error.strerror}')
            return None

    def write_block(self, address, offset, data):
        try:
            self.bus.write_i2c_block_data(address, offset, data)
            return True
        except AttributeError:
            logging.error('Port is not open.')
            return None
        except OSError as error:
            logging.error('Not able to write.')
            logging.error(f'{error.strerror}')
            return None

    def write_word(self, address, reg, value):
        """Write register with specified word value

        Write specified word value in register using smbus write_byte_data method
        Args:
            reg: PCA9685 register
            value: word value to write
        """
        self.bus.write_byte(address, reg, value & 0xFF)
        self.bus.write_byte(address, reg+1, value >> 8)

    def read_byte(self, address, offset):
        try:
            return self.bus.read_byte_data(address, offset)
        except AttributeError:
            logging.error('Port is not open.')
            return None
        except OSError as error:
            logging.error('Not able to read.')
            logging.error(f'{error.strerror}')
            return None

    def read_word(self, address, offset):
        high = self.read_byte(address, offset)
        low = self.read_byte(address, offset+1)
        if high != None and low != None:
            return (high << 8) + low
        return None

    def read_word_2c(self, address, offset):
        val = self.read_word(address, offset)
        if val != None:
            if val >= 0x8000:
                return -((65535 - val) + 1)
            return val
        else:
            return None

    def read_block(self, address, offset, bytes):
        try:
            return self.bus.read_i2c_block_data(address, offset, bytes)
        except AttributeError:
            logging.error('Port is not open')
            return None
        except OSError as error:
            logging.error('Not able to read.')
            logging.error(f'{error.strerror}')
            return None

if __name__ == '__main__':
    i2c = BusI2C(1)
    i2c.write_byte(0, 0, 0)
    print(i2c.read_byte(0, 0))
