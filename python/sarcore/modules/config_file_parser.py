#! /usr/bin/env python3

"""config_file_parser

This module reads configurations from sarcore.conf or other file pased as
argument to contructor of ConfigFileParser
"""

from __future__ import annotations

import os
import configparser

from types import SimpleNamespace
from typing import List, Tuple

import sarcore as sc

class ConfigFileParser:
    """Class to get options from a file and create a SimpleNamespace named
    options.

    Args:
        file: path where config file will be looked

    Attributes:
        options: SimpleNamespace to store all configurations
        options_setters: Dictionary to link each option with its \
                respective setter
        config: Instance of configparser to process config file

    """
    def __init__(self, _file:str|None):
        self.options = SimpleNamespace()

        self.options.app_name = ''
        self.options.app_version = ''


        self.options_setters = {
                'imu': self.set_option_imu,
                'battery': self.set_option_battery,
                'pwm': self.set_option_pwm,
                'camera': self.set_option_camera,
                'i2c_port': self.set_option_i2c_port,
                'display_title': self.set_option_display_title,
                'display_angles': self.set_option_display_angles,
                'intensity': self.set_option_intensity,
                'screen_height': self.set_option_screen_height,
                'screen_width': self.set_option_screen_width,
                'channels': self.set_option_channels,
                'display_horizon': self.set_option_display_horizon,
                'horizon_thickness': self.set_option_horizon_thickness,
                'horizon_color': self.set_option_horizon_color,
                'battery_capacity': self.set_option_battery_capacity,
                'battery_soc': self.set_option_battery_soc,
                'debug_level': self.set_option_debug_level,
                }

        config_files = self.add_file_to_priority_list(None)
        _file, save_enabled = self.search_file(config_files)

        self.config = configparser.ConfigParser()
        self.config.read(_file)

        self.parse_config()

        self.options.config_path = os.path.dirname(os.path.abspath(_file))
        self.options.file = _file
        self.options.save_enabled = save_enabled

    @classmethod
    def add_file_to_priority_list(cls, _file:str|None) -> List:
        """Create a list of priority files and paths starting with given file

        To load the configurations from a file there are two default locations:

        1. ~/.config/sarcore/sarcore.conf
        2. /usr/local/lib/python3.9/dist-packages/sarcore/sarcore.conf

        Note:
            Location may change upon python version or virtual environment \
                    configuration.

        If a file is provided, it will go to the top of the list.

        Args:
            file: name and path of file where configurations are saved. If \
                    it is None the list remainds with default files.


        Returns:
            Configuration files list: The list with two or three names and \
                    paths of files where to look for configurations.

        """
        config_files = [
                os.path.join(os.environ['HOME'],
                    '.config',
                    'sarcore',
                    'sarcore.conf'),
                os.path.join(os.path.dirname(os.path.dirname(sc.__file__)),
                    'sarcore',
                    'sarcore.conf')
                ]

        if _file is not None:
            config_files.insert(0, file)

        return config_files

    @classmethod
    def search_file(cls, files:List) -> Tuple[str|None, bool]:
        """Looks for a configuration file in a given list of files

        This method iterate through the list of files and its stop at \
                first file that can be opened

        Args:
            files: List of names and path of files where to look

        Returns:
            file: Name and path of available config file. None if not file \
                    available.
            save_enabled: True when path is write enabled, False otherwise

        """
        save_enabled = False

        for _file in files:
            if os.path.exists(_file):

                if _file == files[-1]:
                    save_enabled = False
                else:
                    save_enabled = True

                return _file, save_enabled

        return None, save_enabled

    def parse_config(self) -> None:
        """Iterate through Sections and options parsed from config file

        This method uses the dictionary given by configparser module. Iterates \
                each section and options. Later uses items to set each option \
                in the SimpleNamespace using the corresponding setter.

        """
        for section in self.config.sections():
            for name, value in self.config.items(section):
                setter = self.options_setters.get(name)
                if setter is not None:
                    setter(value)

    def set_option_imu(self, value:str) -> None:
        """Set imu option

        Args:
            value: 'emulated' to use mocked sensor. 'true' to use IMU connected \
                    to i2c bus. 'false' to leave IMU unused.

        """
        value = str(value).lower()

        if value == 'emulated':
            self.options.imu = 'emulated'
        elif value == 'true':
            self.options.imu = True
        else:
            self.options.imu = False

    def set_option_battery(self, value:str) -> None:
        """Set battery option

        Args:
            value: 'emulated' to use mocked sensor. 'true' to use power \
                    sensor connected to i2c bus. 'false' to leave power \
                    sensor unused.

        """
        value = str(value).lower()

        if value == 'emulated':
            self.options.battery = 'emulated'
        elif value == 'true':
            self.options.battery = True
        else:
            self.options.battery = False

    def set_option_pwm(self, value:str) -> None:
        """Set pwm option

        Args:
            value: 'true' to use pmw sensor connected to i2c bus. 'false' to \
                    leave pwm sensor unused.

        """
        value = str(value).lower()

        if value == 'true':
            self.options.pwm = True
        else:
            self.options.pwm = False

    def set_option_camera(self, value:str|int) -> None:
        """Set camera option

        Args:
            value: 'false' to use gray frame. String with name and path of a
                    camera to use it. if value is an integer '/dev/video<value>'
                    will be stored.


        """
        value = str(value).lower()

        if value.isnumeric():
            value = ''.join(['/dev/video', value])

        if value == 'false':
            self.options.camerapath = False
        else:
            self.options.camerapath = value

    def set_option_i2c_port(self, value:str) -> None:
        """Set i2c option

        Args:
            value: port where i2c bus will be looked. (Ex. 1 for /dev/i2c1)

        """
        value = str(value).lower()

        if not value.isnumeric():
            value = '1'

        self.options.i2c_port = int(value)

    def set_option_display_title(self, value:str) -> None:
        """Set display title option

        When Borescope App is running and the camera is active, title and \
                versión of application can be shown at the top of the screen.

        Args:
            value: 'true' to show title, 'false' otherwise.

        """
        value = str(value).lower()

        if value == 'true':
            self.options.display_title = True
        else:
            self.options.display_title = False

    def set_option_display_angles(self, value:str) -> None:
        """Set display angles option

        When Borescope App is running and the camera is active, Euler's \
                angles are shown in bottom-right corner of the screen.

        Args:
            value: 'true' to show angles, 'false' otherwise.

        """
        value = str(value).lower()

        if value == 'true':
            self.options.display_angles = True
        else:
            self.options.display_angles = False

    def set_option_intensity(self, value:str) -> None:
        """Set LED intensity option

        When Borescope App is running and the camera is active, LEDs can be \
                controled. This method is used to store current value

        Args:
            value: LED intensity in range 0-100

        """
        value = str(value)

        if value.isnumeric():
            value = int(value)
        else:
            value = 0

        if value > 100:
            value = 100

        if value < 0:
            value = 0

        self.options.intensity = value

    def set_option_screen_height(self, value:str) -> None:
        """Set screen height option

        Args:
            value: height of app screen. If value is wrong somehow defaults
                will be 320

        """
        value = str(value)

        if value.isnumeric():
            value = int(value)
        else:
            value = 320

        if value < 0:
            value = 320

        self.options.screen_height = value

    def set_option_screen_width(self, value:str) -> None:
        """Set screen width option

        Args:
            value: width of app screen. If value is wrong somehow defaults
                will be 480

        """
        value = str(value)

        if value.isnumeric():
            value = int(value)
        else:
            value = 480

        if value < 0:
            value = 480

        self.options.screen_width = value

    def set_option_channels(self, value:str) -> None:
        """Set video channels option

        Args:
            value: channels of video frame (1, 3 or 4). If value is wrong
                somehow defaults will be 3

        """
        value = str(value)

        if value.isnumeric():
            value = int(value)
        else:
            value = 3

        if value not in [1, 3, 4]:
            value = 3

        self.options.channels = value

    def set_option_display_horizon(self, value:str) -> None:
        """Set display horizon option

        When Borescope App is running and the camera is active, artificial \
                horizon can be shown.

        Args:
            value: 'true' to show artificial horizon, 'false' otherwise.

        """
        value = str(value).lower()

        if value == 'true':
            self.options.display_horizon = True
        else:
            self.options.display_horizon = False

    def set_option_horizon_thickness(self, value:int) -> None:
        """Set artificial horizon thickness option

        Args:
            value: thickness in pixels of artificial horizon (range 1-5). If
                value is wrong somehow defaults will be 1

        """
        value = str(value)

        if value.isnumeric():
            value = int(value)
        else:
            value = 1

        if value < 1:
            value = 1

        if value > 5:
            value = 5

        self.options.horizon_thickness = value

    def set_option_battery_capacity(self, value:int) -> None:
        value = str(value)

        if value.isnumeric():
            value = int(value)
        else:
            value = 5000

        if value < 1:
            value = 1

        if value > 5000:
            value = 5000

        self.options.battery_capacity = value

    def set_option_battery_soc(self, value:int) -> None:
        value = str(value)

        if value.isnumeric():
            value = int(value)
        else:
            value = 5000

        if value < 1:
            value = 1

        if value > 5000:
            value = 5000

        self.options.battery_soc = value

    def set_option_debug_level(self, value:int) -> None:
        value = str(value)

        if value.isnumeric():
            value = int(value)
        else:
            value = 0

        if value < 0:
            value = 0

        if value > 3:
            value = 3

        self.options.debug_level = value

    def set_option_horizon_color(self, value:str|Tuple|List) -> None:
        """Set artificial horizon color option

        Args:
            value: color of artificial horizon in RGB map separated by hyphens.
                If value is wrong somehow defaults will be green (a list with
                [0, 255, 0]). List or Tuples with values are also allow. If the
                list has floats these will be truncated

        """

        if type(value) is not str:
            if type(value) in [tuple, list] and len(value) == 3:
                value = '%s-%s-%s' % tuple(value)
            else:
                value = '0-255-0'

        try:
            value = value.replace(' ', '-')
            red, green, blue = map(lambda x: int(float(x)), value.split('-'))
        except ValueError:
            red, green, blue = (0, 255, 0)

        if red not in range(256):
            red = 0

        if green not in range(256):
            green = 255

        if blue not in range(256):
            blue = 0

        self.options.horizon_color = [red, green, blue]

    def get_options(self):
        """Return all config options

        Returns:
            options: SimpleNamespace with options loaded from config file

        """
        return self.options
