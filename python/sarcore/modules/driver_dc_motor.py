#! /usr/bin/env python3

from sarcore.modules.device_pca9685 import Pca9685


class DCMotor:
    def __init__(self, options):
        self.options = options

        self.device = None
        self.available = False
        self.direction = None

        self.device_list = [
                Pca9685,
                ]

        self._duty = 0
        self.disable()

    def discover(self):
        self.device = None
        if self.options.pwm is True and self.options.running_on_arm:
            for device in self.device_list:
                self.device = device()
                self.device.discover()
                if self.device.discovered:
                    self.available = True
                    break
            else:
                self.device = None

            if self.device is not None:
                self.device.setup()
                self.device.set_min_off_time = 0
                self.device.set_max_off_time = 4095

    def set_duty(self, channel, duty):
        # 0-100
        if self.available:
            self._duty = duty
            self.device.set_duty(channel, duty)
            return duty
        return None

    def set_direction(self, direction):
        # acá va el uso de la gpio
        # adelante = 'D'   atras = 'R'
        if direction == 'D':
            pass
        else:
            pass

    def disable(self):
        if self.available:
            self.device.disable_output()

    def enable(self):
        if self.available:
            self.device.enable_output()


if __name__ == '__main__':
    from types import SimpleNamespace
    import time
    import platform

    options = SimpleNamespace()
    options.pwm = True
    if platform.machine() in ['armv7l', 'aarch64']:
        options.running_on_arm = True
    else:
        options.running_on_arm = False

    motor = DCMotor(options)
    motor.discover()
    motor.enable()
    channel = 10 # ver que canal es
    delay = 0.1
    while 1:
        # cambiar dirección
        motor.set_direction('D') # adelante = 'D'   atras = 'R'
        for duty in range(0,100):
            duty = motor.set_duty(channel,duty)
            print(duty)
            time.sleep(delay)
        for duty in range(100,0,-1):
            duty = motor.set_duty(channel,duty)
            print(duty)
            time.sleep(delay)

        # cambiar dirección
        motor.set_direction('R') # adelante = 'D'   atras = 'R'
        for duty in range(0,100):
            duty = motor.set_duty(channel,duty)
            print(duty)
            time.sleep(delay)
        for duty in range(100,0,-1):
            duty = motor.set_duty(channel,duty)
            print(duty)
            time.sleep(delay)

